package karnor.primenumber.controller;

import karnor.primenumber.service.PrimeCheckerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigInteger;

import static java.util.Objects.nonNull;

@Slf4j
@RestController
public class PrimeController {

    private final PrimeCheckerService primeCheckerService;

    public PrimeController(PrimeCheckerService primeCheckerService) {
        this.primeCheckerService = primeCheckerService;
    }

    @GetMapping("/api/test/{prime}")
    public ResponseEntity<Boolean> isPrime(@PathVariable BigInteger prime) {
        if (nonNull(prime)) {
            return ResponseEntity.ok(primeCheckerService.checkIfPrime(prime));
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Number must not be null!");
        }
    }
}
