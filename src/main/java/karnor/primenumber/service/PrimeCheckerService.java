package karnor.primenumber.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigInteger;

@Slf4j
@Service
public class PrimeCheckerService {

    private static final int CERTAINTY = 200;

    public boolean checkIfPrime(BigInteger number) {
        log.info("Checking number = {}", number);
        return number.isProbablePrime(CERTAINTY);
    }
}
