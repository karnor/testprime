package karnor.primenumber.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PrimeControllerTest {

    public static final String FALSE = "false";
    public static final String TRUE = "true";
    private static final String PRIME_1332_DIGITS = "285542542228279613901563566102164008326164238644702889199247456602" +
            "284400390600653875954571505539843239754513915896150297878399377056071435169747221107988791198200988477531" +
            "33921428277201605900990458668625498908481573542248040902234429758835252600438389063261612407631738741688" +
            "114859248618836187390417578314569601691957439076559828018859903557844859107768367717552043407428772657800" +
            "62667596159707595213278285556627816783856915818444364448125115624281367424904593632128101802760960881114" +
            "01003377570363545725120924073646921576797146199387619296560302680261790118132925012323046444438622308877" +
            "924609373773012481681672424493674474488537770155783006880852648161513067144814790288366664062257274665275" +
            "7871273746492310963750011709018907862633246195787957314256938050730561196775803380843333819875009029688319" +
            "3591309526982131114132239335649017848872898228815628260081383129614366384594543114404375382154287127774560" +
            "64478585641592133284435802064227146949130917627164470416896780700967735904298089096167504529272580008435003" +
            "448316282970899027286499819943876472345742762637296948483047509171741861811306885187927486226122933413689280" +
            "566343844666463265724761672756608391056505289757138993202111214957953114279462545533053870678210676017687509" +
            "77866100460014602138408448021225053689054793742003095722096732954750721718115531871310231057902608580607";

    private static final String NOT_PRIME_1332_DIGITS = "285542542228279613901563566102164008326164238644702889199247456602" +
            "284400390600653875954571505539843239754513915896150297878399377056071435169747221107988791198200988477531" +
            "33921428277201605900990458668625498908481573542248040902234429758835252600438389063261612407631738741688" +
            "114859248618836187390417578314569601691957439076559828018859903557844859107768367717552043407428772657800" +
            "62667596159707595213278285556627816783856915818444364448125115624281367424904593632128101802760960881114" +
            "01003377570363545725120924073646921576797146199387619296560302680261790118132925012323046444438622308877" +
            "924609373773012481681672424493674474488537770155783006880852648161513067144814790288366664062257274665275" +
            "7871273746492310963750011709018907862633246195787957314256938050730561196775803380843333819875009029688319" +
            "3591309526982131114132239335649017848872898228815628260081383129614366384594543114404375382154287127774560" +
            "64478585641592133284435802064227146949130917627164470416896780700967735904298089096167504529272580008435003" +
            "448316282970899027286499819943876472345742762637296948483047509171741861811306885187927486226122933413689280" +
            "566343844666463265724761672756608391056505289757138993202111214957953114279462545533053870678210676017687509" +
            "77866100460014602138408448021225053689054793742003095722096732954750721718115531871310231057902608580608";

    private static final String HUGE_PRIME_HEX = "0x7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF" +
            "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF" +
            "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF" +
            "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF" +
            "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF" +
            "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF" +
            "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF" +
            "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF" +
            "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF" +
            "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF" +
            "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";
    public static final CharSequence[] BAD_REQUEST = {"Bad Request", "400"};
    @LocalServerPort
    private int port;
    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void shouldReturnTrueForDecimals() throws Exception {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/api/test/2",
                String.class)).contains(TRUE);
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/api/test/3",
                String.class)).contains(TRUE);
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/api/test/" + PRIME_1332_DIGITS,
                String.class)).contains(TRUE);
    }

    @Test
    public void shouldReturnTrueForHex() throws Exception {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/api/test/0x2",
                String.class)).contains(TRUE);
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/api/test/0X3",
                String.class)).contains(TRUE);
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/api/test/" + HUGE_PRIME_HEX,
                String.class)).contains(TRUE);
    }

    @Test
    public void shouldReturnFalseForDecimals() throws Exception {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/api/test/4",
                String.class)).contains(FALSE);
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/api/test/6",
                String.class)).contains(FALSE);
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/api/test/09",
                String.class)).contains(FALSE);
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/api/test/-42",
                String.class)).contains(FALSE);
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/api/test/0",
                String.class)).contains(FALSE);
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/api/test/"
                + NOT_PRIME_1332_DIGITS, String.class)).contains(FALSE);
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/api/test/"
                + PRIME_1332_DIGITS + PRIME_1332_DIGITS, String.class)).contains(FALSE);

    }

    @Test
    public void shouldReturnFalseForHex() throws Exception {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/api/test/0X4",
                String.class)).contains(FALSE);
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/api/test/0x6",
                String.class)).contains(FALSE);
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/api/test/"
                + HUGE_PRIME_HEX + HUGE_PRIME_HEX.substring(2), String.class)).contains(FALSE);
    }

    @Test
    public void shouldReturnBadRequestStatus() throws Exception {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/api/test/0w4",
                String.class)).contains(BAD_REQUEST);
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/api/test/888d",
                String.class)).contains(BAD_REQUEST);
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/api/test/80x8",
                String.class)).contains(BAD_REQUEST);
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/api/test/ ",
                String.class)).contains(BAD_REQUEST);
    }



}