# PRIME NUMBER CHECKER

SERVICE CHECKS IF NUMBER IS PRIME

## BUILD DOCKER IMAGE
(TESTED ON LINUX.)

run ./mvnw spring-boot:build-image 

It has to be run from main project directory. (Docker demon required).


## Usage

docker run -p 8080:8080 primenumber:0.0.1-SNAPSHOT

## License
GPLv3 license